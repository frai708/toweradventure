// Fill out your copyright notice in the Description page of Project Settings.


#include "CombatUIWidget.h"

void UCombatUIWidget::BeginMakeDecision(UGameCharacter* character)
{
    this->currentTarget = character;
    this->finishedDecision = false;
    
    ShowActionsPanel(character);
}

bool UCombatUIWidget::MakeDecision(float DeltaSeconds)
{
    return this->finishedDecision;
}

TArray<UGameCharacter*> UCombatUIWidget::GetCharacterTargets()
{   
    if (this->currentTarget->isPlayer)
        return this->currentTarget->combatInstance->enemyParty;
    else
        return this->currentTarget->combatInstance->playerParty;
    
}

void UCombatUIWidget::AttackTarget(UGameCharacter* target)
{
    TestCombatAction* action = new TestCombatAction(target);
    this->currentTarget->combatAction = action;
    this->finishedDecision = true;
}

