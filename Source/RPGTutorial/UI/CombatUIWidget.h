// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "RPGTutorial/Characters/GameCharacter.h"
#include "RPGTutorial/Interfaces/IDecisionMaker.h"
#include "RPGTutorial/Tests/TestCombatAction.h"
#include "RPGTutorial/CombatEngine.h"
#include "CombatUIWidget.generated.h"


/**
 * 
 */
UCLASS()
class RPGTUTORIAL_API UCombatUIWidget : public UUserWidget, public IDecisionMaker
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintImplementableEvent, Category = "CombatUI")
	void AddPlayerCharacterPanel(UGameCharacter* target);

	UFUNCTION(BlueprintImplementableEvent, Category = "CombatUI")
	void AddEnemyCharacterPanel(UGameCharacter* target);

	UFUNCTION(BlueprintImplementableEvent, Category = "CombatUI")
	void ShowActionsPanel(UGameCharacter* target);
	
	// Inherited via IDecisionMaker
	void BeginMakeDecision(UGameCharacter* character) override;

	bool MakeDecision(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable, Category = "Combat UI")
	TArray<UGameCharacter*> GetCharacterTargets();

	UFUNCTION(BlueprintCallable, Category ="Combat UI")
	void AttackTarget(UGameCharacter* target);
protected:
	UGameCharacter* currentTarget;
	bool finishedDecision;
};
