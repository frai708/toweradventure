// Fill out your copyright notice in the Description page of Project Settings.


#include "GameCharacter.h"
#include "RPGTutorial/Interfaces/IDecisionMaker.h"
#include "RPGTutorial/Interfaces/ICombatAction.h"


UGameCharacter* UGameCharacter::CreateGameCharacter(FCharacterInfo* characterInfo, UObject* other)
{
	UGameCharacter* character = NewObject<UGameCharacter>(other);
	
	if (!character)
	{
		UE_LOG(LogTemp, Error, TEXT("Character classes datatable not found"));
		return nullptr;
	}

	//locate character classes asset
	UDataTable* characterClasses = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), 
																		nullptr, 
																	TEXT("Engine.DataTable'/Game/Blueprints/DataTables/DT_CHaracterClasses.DT_CHaracterClasses'")));
	if (!characterClasses)
	{
		UE_LOG(LogTemp, Error, TEXT("Character classes datatable not found"));
	}
	else
	{
		character->CharacterName = characterInfo->Character_Name;
		FCharacterClassInfo* row = characterClasses->FindRow<FCharacterClassInfo>(*(characterInfo->Class_ID), TEXT("LookupCharacterClass"));
		character->ClassInfo = row;
		character->MHP = character->ClassInfo->StartMHP;
		character->MMP = character->ClassInfo->StartMMP;
		character->HP = character->MHP;
		character->MP = character->MMP;

		character->ATK = character->ClassInfo->StartATK;
		character->DEF = character->ClassInfo->StartDEF;
		character->LUCK = character->ClassInfo->StartLuck;
		character->XP = character->ClassInfo->XP;
		character->MXP = character->ClassInfo->MXP;
		character->Lvl = character->ClassInfo->Lvl;
		character->LearnedAbilities = character->ClassInfo->LearnedAbillities;
		character->isPlayer = true;
	}		
	return character;
}

UGameCharacter* UGameCharacter::CreateGameCharacter(FEnemyInfo* enemyInfo, UObject* outer)
{
	UGameCharacter* character = NewObject<UGameCharacter>(outer);

	character->CharacterName = enemyInfo->Class_Name;
	character->ClassInfo = nullptr;
	character->MHP = enemyInfo->StartMHP;
	character->MMP = 0;
	character->HP = enemyInfo->StartMHP;
	character->MP = 0;

	character->ATK = enemyInfo->StartATK;
	character->DEF = enemyInfo->StartDEF;
	character->LUCK = enemyInfo->StartLuck;
	character->Gold = enemyInfo->Gold;
	character->XP = enemyInfo->XP;
    character->decisionMaker = new TestDecisionMaker();
	character->isPlayer = false;
	
	return character;
}

UGameCharacter* UGameCharacter::SelectTarget()
{
	UGameCharacter* target = nullptr;
	TArray<UGameCharacter*>targetList = this->combatInstance->enemyParty;
	if (!this->isPlayer)
	{
		targetList = this->combatInstance->playerParty;
	}
	for (int i = 0; i < targetList.Num(); i++)
	{
		if (targetList[i]->HP > 0)
		{
			target = targetList[i];
			break;
		}
	}
	if (target->HP <= 0)
	{
		return nullptr;
	}
	return target;
}

void UGameCharacter::BeginDestroy()
{
	Super::BeginDestroy();
	if(!this->isPlayer)
		delete(this->decisionMaker);
	
}

void UGameCharacter::BeginMakeDecision()
{
	this->decisionMaker->BeginMakeDecision(this);
}

bool UGameCharacter::MakeDecision(float DeltaSeconds)
{
	return this->decisionMaker->MakeDecision(DeltaSeconds);
}

void UGameCharacter::BeginExecuteAction()
{
	this->combatAction->BeginExecuteAction(this);
}

bool UGameCharacter::ExecuteAction(float DeltaSeconds)
{
	bool finishedAction = this->combatAction->ExecuteAction(DeltaSeconds);
	if (finishedAction)
	{
		delete(this->combatAction);
		return true;
	}
	return false;
}
