// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "RPGTutorial/Interfaces/ControllableCharacter.h"
#include "RPGCharacter.generated.h"



UCLASS()
class RPGTUTORIAL_API ARPGCharacter : public ACharacter, public IControllableCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARPGCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	//virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void MoveVertical(float Value);
	virtual  void MoveHorizontal(float Value);

};
