// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RPGTutorial/CombatEngine.h"
#include "RPGTutorial/DataTable/EnemyInfo.h"
#include "RPGTutorial/DataTable/CharacterInfo.h"
#include "RPGTutorial/DataTable/CharacterClassInfo.h"
#include "RPGTutorial/Tests/TestCombatAction.h"
#include "RPGTutorial/Tests/TestDecisionMaker.h"
#include "UObject/NoExportTypes.h"
#include "GameCharacter.generated.h"

/**
 * 
 */
class ICombatAction;
class IDecisionMaker;
class CombatEngine;

UCLASS(BlueprintType)
class RPGTUTORIAL_API UGameCharacter : public UObject
{
	GENERATED_BODY()
public:
	FCharacterClassInfo* ClassInfo;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	FString CharacterName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	int32 MHP;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	int32 MMP;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	int32 HP;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	int32 MP;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	int32 ATK;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	int32 DEF;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	int32 LUCK;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
	int32 Gold;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	int32 XP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	int32 MXP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	int32 Lvl;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterInfo")
	TArray<FString> LearnedAbilities;

	CombatEngine* combatInstance;
	ICombatAction* combatAction;
	IDecisionMaker* decisionMaker;

	bool isPlayer;

	static UGameCharacter* CreateGameCharacter(FCharacterInfo* characterInfo, UObject* other);
	static UGameCharacter* CreateGameCharacter(FEnemyInfo* enemyInfo, UObject* outer);

	UGameCharacter* SelectTarget();

	void BeginDestroy() override;

	void BeginMakeDecision();
	bool MakeDecision(float DeltaSeconds);

	void BeginExecuteAction();
	bool ExecuteAction(float DeltaSeconds);

	

	

};
