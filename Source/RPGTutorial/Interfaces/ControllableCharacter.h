// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "UObject/UObjectGlobals.h"
#include "ControllableCharacter.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UControllableCharacter : public UInterface
{
	GENERATED_BODY()

public:
	UControllableCharacter();

};

/**
 * 
 */
class RPGTUTORIAL_API IControllableCharacter
{
	GENERATED_BODY()

public:
	virtual void MoveVertical(float Value);
	virtual void MoveHorizontal(float Value);
};
