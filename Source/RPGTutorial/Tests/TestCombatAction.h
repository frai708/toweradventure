// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RPGTutorial/RPGTutorial.h"
#include "RPGTutorial/Interfaces/ICombatAction.h"
#include "RPGTutorial/Characters/GameCharacter.h"

/**
 * 
 */

class TestCombatAction : public ICombatAction
{
protected:
	float delayTimer;
	UGameCharacter* character;
	UGameCharacter* target;
public:
	TestCombatAction(UGameCharacter* Target);
	virtual void BeginExecuteAction(UGameCharacter* Character) override;
	virtual bool ExecuteAction(float DeltaSeconds) override;
};
