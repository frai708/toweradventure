// Fill out your copyright notice in the Description page of Project Settings.


#include "TestCombatAction.h"

TestCombatAction::TestCombatAction(UGameCharacter* Target)
{
	this->target = Target;
}

void TestCombatAction::BeginExecuteAction(UGameCharacter* Character)
{
	this->character = Character;
	//target is dead, select another target
	if (this->target->HP <= 0)
	{
		this->target = this->character->SelectTarget();
	}
	//no target, just return
	if (this->target == nullptr)
	{
		return;
	}

	UE_LOG(LogTemp, Log, TEXT("%s attack %s"), *character->CharacterName, *target->CharacterName);
	
	target->HP -= (character->ATK - target->DEF) >= 0 ? (character->ATK - target->DEF) : 0;

	this->delayTimer = 1.f;
}

bool TestCombatAction::ExecuteAction(float DeltaSeconds)
{
	this->delayTimer -= DeltaSeconds;
	return this->delayTimer <=0.f;
}
