// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RPGTutorial/RPGTutorial.h"
#include "TestCombatAction.h"
#include "RPGTutorial/Interfaces/IDecisionMaker.h"
#include "RPGTutorial/Characters/GameCharacter.h"
/**
 * 
 */
class RPGTUTORIAL_API TestDecisionMaker : public IDecisionMaker
{
public:


	// Inherited via IDecisionMaker
	virtual void BeginMakeDecision(UGameCharacter* character) override;

	virtual bool MakeDecision(float DeltaSeconds) override;

};
