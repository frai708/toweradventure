// Fill out your copyright notice in the Description page of Project Settings.


#include "TestDecisionMaker.h"

void TestDecisionMaker::BeginMakeDecision(UGameCharacter* character)
{
	//pick a target
	UGameCharacter* target = character->SelectTarget();
	//UE_LOG(LogTemp, Log, TEXT("Character %s decision into TestDecisionMaker"), *character->CharacterName );
	character->combatAction = new TestCombatAction(target);
}

bool TestDecisionMaker::MakeDecision(float DeltaSeconds)
{
	return true;
}
