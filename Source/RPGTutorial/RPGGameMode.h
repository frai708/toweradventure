// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <Kismet\GameplayStatics.h>
#include "CoreMinimal.h"
#include "RPGGameInstance.h"
#include "CombatEngine.h"
#include "GameFramework/GameModeBase.h"
#include "UI/CombatUIWidget.h"
#include "RPGTutorial/Controllers/RPGPlayerController.h"
#include "RPGTutorial/Characters/RPGCharacter.h"
#include "RPGGameMode.generated.h"

/**
 * 
 */
UCLASS()
class RPGTUTORIAL_API ARPGGameMode : public AGameModeBase
{
	GENERATED_BODY()
private:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

public:
	CombatEngine* currentCombatInstance;
	UPROPERTY()
	TArray<UGameCharacter*> enemyParty;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UI")
	TSubclassOf<class UUserWidget> GameOverUIClass;

	UPROPERTY()
	UCombatUIWidget* CombatUIInstance;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="UI")
	TSubclassOf<class UCombatUIWidget> CombatUIClass;

	//ARPGGameMode();

	ARPGGameMode(const class FObjectInitializer& ObjectInitializer);

	UFUNCTION(exec)
	void TestCombat();
	
};
