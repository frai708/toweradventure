// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RPGTutorial.h"
#include "RPGTutorial/Characters/GameCharacter.h"

class UGameCharacter;
enum class CombatPhase : uint8
{
	CPHASE_Decision,
	CPHASE_Action,
	CPHASE_Victory,
	CPHASE_GameOver,
};

class RPGTUTORIAL_API CombatEngine
{
public:
	TArray<UGameCharacter*> combatantOrder;
	TArray<UGameCharacter*> playerParty;
	TArray<UGameCharacter*> enemyParty;

	CombatPhase phase;
	int32 GoldTotal;
	int32 XPTotal;

	bool waitingForCharacter;

	CombatEngine(TArray<UGameCharacter*> playerParty, TArray<UGameCharacter*> enemyParty);
	~CombatEngine();

	bool Tick(float DeltaSeconds);

protected:
	
	UGameCharacter* currentTickTarget;
	int32 tickTargetIndex;

	void SetPhase(CombatPhase Phase);
	void SelectNextCaracter();
};
