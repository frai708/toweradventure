// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataTable.h"
#include "EnemyInfo.generated.h"

USTRUCT(BlueprintType)
struct FEnemyInfo : public FTableRowBase
{
	GENERATED_USTRUCT_BODY();
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ClassInfo")
	FString Class_Name;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ClassInfo")
	int32 StartMHP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ClassInfo")
	int32 StartMMP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ClassInfo")
	int32 StartATK;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ClassInfo")
	int32 StartDEF;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ClassInfo")
	int32 StartLuck;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ClassInfo")
	TArray<FString> StartingAbilities;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "EnemyInfo")
	int32 Gold;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyInfo")
	int32 XP;
	


};
