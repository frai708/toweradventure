// Fill out your copyright notice in the Description page of Project Settings.


#include "RPGGameMode.h"


void ARPGGameMode::BeginPlay()
{
	Super::BeginPlay();
	Cast<URPGGameInstance>(GetGameInstance())->Init();
}

void ARPGGameMode::Tick(float DeltaSeconds)
{
	
	
	if (this->currentCombatInstance)
	{
		bool combatOver = this->currentCombatInstance->Tick(DeltaSeconds);
		if (combatOver)
		{
			if (this->currentCombatInstance->phase == CombatPhase::CPHASE_GameOver)
			{
				UE_LOG(LogTemp, Log, TEXT("Player loses combat, game over"));
				Cast<URPGGameInstance>(GetGameInstance())->PrepareReset();

				UUserWidget* GameOverUIInstance = CreateWidget<UUserWidget>(GetGameInstance(), this->GameOverUIClass);
				GameOverUIInstance->AddToViewport();
			}
			else if (this->currentCombatInstance->phase == CombatPhase::CPHASE_Victory)
			{
				UE_LOG(LogTemp, Log, TEXT("Player wins combat"));
				URPGGameInstance* gameInstance = Cast<URPGGameInstance>(GetGameInstance());
				gameInstance->GameGold += this->currentCombatInstance->GoldTotal;

				for (int i = 0; i < gameInstance->PartyMembers.Num(); ++i)
				{
					gameInstance->PartyMembers[i]->XP += this->currentCombatInstance->XPTotal;

					if (gameInstance->PartyMembers[i]->XP >= gameInstance->PartyMembers[i]->MXP)
					{
						gameInstance->PartyMembers[i]->Lvl++;
						gameInstance->PartyMembers[i]->MHP++;
						gameInstance->PartyMembers[i]->MMP++;
						gameInstance->PartyMembers[i]->ATK++;
						gameInstance->PartyMembers[i]->DEF++;
						gameInstance->PartyMembers[i]->LUCK++;
						gameInstance->PartyMembers[i]->MXP +=
							gameInstance->PartyMembers[i]->MXP;
					}
				}
			}
			UGameplayStatics::GetPlayerController(GetWorld(), 0)->bShowMouseCursor = false;

			//enable player actor
			UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetActorTickEnabled(true);

			this->CombatUIInstance->RemoveFromParent();
			this->CombatUIInstance = nullptr;

			for (int i = 0; i < this->currentCombatInstance->playerParty.Num(); i++)
			{
				this->currentCombatInstance->playerParty[i]->decisionMaker = nullptr;
			}

			delete(this->currentCombatInstance);
			this->currentCombatInstance = nullptr;
			this->enemyParty.Empty();
		}
	}


}



ARPGGameMode::ARPGGameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PlayerControllerClass = ARPGPlayerController::StaticClass();
	DefaultPawnClass = ARPGCharacter::StaticClass();
}

void ARPGGameMode::TestCombat()
{
	


	//locate enemies asset
	UDataTable* enemyTable = Cast<UDataTable>(StaticLoadObject(
		UDataTable::StaticClass(), nullptr, TEXT("/Script/Engine.DataTable'/Game/Blueprints/DataTables/DT_EnemyInfo.DT_EnemyInfo'")
	));

	if (!enemyTable)
	{
		UE_LOG(LogTemp, Error, TEXT("Enemies data table not found!"));
		return;
	}

	//locale enemy
	FEnemyInfo* row = enemyTable->FindRow<FEnemyInfo>(TEXT("S1"), TEXT("LookupEnemyInfo"));
	
	if (!row)
	{
		UE_LOG(LogTemp, Error, TEXT("Enemy ID 'S1' not found!"));
		return;
	}

	//disable player actor
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetActorTickEnabled(false);

	//add character to enemy party
	UGameCharacter* enemy = UGameCharacter::CreateGameCharacter(row, this);
	this->enemyParty.Add(enemy);

	URPGGameInstance* gameInstance = Cast<URPGGameInstance>(GetGameInstance());
	this->currentCombatInstance = new CombatEngine(gameInstance->PartyMembers, this->enemyParty);

	this->CombatUIInstance = CreateWidget<UCombatUIWidget>(GetGameInstance(), this->CombatUIClass);
	this->CombatUIInstance->AddToViewport();

		if(CombatUIInstance)
		 UE_LOG(LogTemp, Log, TEXT(" widget"));
		
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->bShowMouseCursor = true;
	
	for (int i = 0; i < gameInstance->PartyMembers.Num(); i++)
	{
		this->CombatUIInstance->AddPlayerCharacterPanel(gameInstance->PartyMembers[i]);
		gameInstance->PartyMembers[i]->decisionMaker = this->CombatUIInstance;
	}


	
	for (int i = 0; i < this->enemyParty.Num(); i++)
		this->CombatUIInstance->AddEnemyCharacterPanel(this->enemyParty[i]);

	UE_LOG(LogTemp, Log, TEXT("Combat started"));

}


