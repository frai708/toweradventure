// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RPGTutorialGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class RPGTUTORIAL_API ARPGTutorialGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
