// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Components/InputComponent.h"
#include "RPGTutorial/Interfaces/ControllableCharacter.h"
#include "RPGPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class RPGTUTORIAL_API ARPGPlayerController : public APlayerController
{
	GENERATED_BODY()
	
protected:
	void MoveVertical(float Value);
	void MoveHorizontal(float Value);

	virtual void SetupInputComponent() override;
};
