// Fill out your copyright notice in the Description page of Project Settings.


#include "RPGGameInstance.h"


URPGGameInstance::URPGGameInstance(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	isInitialized = false;
}

void URPGGameInstance::Init()
{
	Super::Init();
	
	if (this->isInitialized) return;

	isInitialized = true;
	
	UE_LOG(LogTemp, Warning, TEXT("Initialize Begin!"));
	//locate characters asset
	UDataTable* characters = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), nullptr, TEXT("/Script/Engine.DataTable'/Game/Blueprints/DataTables/DT_CharacterInfo.DT_CharacterInfo'")));

	if (!characters)
	{
		UE_LOG(LogTemp, Error, TEXT("Characters data Table not found!"));
		return;
	}

	//locate character
	FCharacterInfo* row = characters->FindRow<FCharacterInfo>(TEXT("S1"), TEXT("LookupCharacterClass"));

	if (!row)
	{
		UE_LOG(LogTemp, Error, TEXT("Character ID 'S1' not found"));
		return;
	}

	//add character to party
	this->PartyMembers.Add(UGameCharacter::CreateGameCharacter(row, this));
	for (int i = 0; i < PartyMembers.Num(); i++)
	{
		UE_LOG(LogTemp, Log, TEXT("%s in PartyMember"), *PartyMembers[i]->CharacterName);
	}

}

void URPGGameInstance::PrepareReset()
{
	this->isInitialized = false;
	this->PartyMembers.Empty();
}
