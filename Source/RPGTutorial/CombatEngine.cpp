// Fill out your copyright notice in the Description page of Project Settings.


#include "CombatEngine.h"



CombatEngine::CombatEngine(TArray<UGameCharacter*> playerParty, TArray<UGameCharacter*> enemyParty)
{
	this->playerParty = playerParty;
	this->enemyParty = enemyParty;

	//first add all players to combat order
	for (int i = 0; i < playerParty.Num(); i++)
	{
		this->combatantOrder.Add(playerParty[i]);
	}

	//next add all enemies to combat order
	for (int i = 0; i < enemyParty.Num(); i++)
	{
		this->combatantOrder.Add(enemyParty[i]);
	}

	for (int i = 0; i < this->combatantOrder.Num(); i++)
	{
		this->combatantOrder[i]->combatInstance = this;
	}

	this->tickTargetIndex = 0;

	this->SetPhase(CombatPhase::CPHASE_Decision);

}

CombatEngine::~CombatEngine()
{
	//free enemies
	for (int i = 0; i < this->enemyParty.Num(); i++)
	{
		this->enemyParty[i] = nullptr;
	}

	for (int i = 0; i < this->combatantOrder.Num(); i++)
	{
		this->combatantOrder[i]->combatInstance = nullptr;
	}
}

bool CombatEngine::Tick(float DeltaSeconds)
{
	switch (phase)
	{
	case CombatPhase::CPHASE_Decision:
	{
		if (!this->waitingForCharacter)
		{
			this->currentTickTarget->BeginMakeDecision();
			this->waitingForCharacter = true;
		}

		bool decisionMade = this->currentTickTarget->MakeDecision(DeltaSeconds);

		if (decisionMade)
		{
			SelectNextCaracter();
			// no next character, switch to action phase
			if (this->tickTargetIndex == -1)
				this->SetPhase(CombatPhase::CPHASE_Action);
		}
	}
		break;
	case CombatPhase::CPHASE_Action:
	{
		if (!this->waitingForCharacter)
		{
			this->currentTickTarget->BeginExecuteAction();
			this->waitingForCharacter = true;
		}

		bool actionFinished = this->currentTickTarget->ExecuteAction(DeltaSeconds);
		if (actionFinished)
		{
			SelectNextCaracter();
			//no next character, switch to action phase
			if (this->tickTargetIndex == -1)
				this->SetPhase(CombatPhase::CPHASE_Decision);
		}
	}	
		break;
		// in case of victory or combat, return true(combat is finished)
	case CombatPhase::CPHASE_GameOver:
	case CombatPhase::CPHASE_Victory:
		return true;
		break;

	}
	//check for game over
	int deadCount = 0;
	for (int i = 0; i < this->playerParty.Num(); i++)
	{
		if (this->playerParty[i]->HP <= 0) deadCount++;
	}

	//all players have died, switch to game over phase
	if (deadCount == this->playerParty.Num())
	{
		this->SetPhase(CombatPhase::CPHASE_GameOver);
		return false;
	}

	//check for victory
	deadCount = 0;
	int32 Gold = 0;
	int32 XP = 0;
	for (int i = 0; i < this->enemyParty.Num(); i++)
	{
		if (this->enemyParty[i]->HP <= 0) deadCount++;
		Gold += this->enemyParty[i]->Gold;
		XP += this->enemyParty[i]->XP;
	}

	//all enemies have died, switch to victory phase
	if (deadCount == this->enemyParty.Num())
	{
	    this->SetPhase(CombatPhase::CPHASE_Victory);
		GoldTotal = Gold;
		XPTotal = XP;
		return false;
	}
	//if execution reaches here, combat has not finished - return false
	return false;
}

void CombatEngine::SetPhase(CombatPhase Phase)
{
	this->phase = Phase;
	switch (Phase)
	{
	case CombatPhase::CPHASE_Action:
	case CombatPhase::CPHASE_Decision:
		//set the active target to the first character in the combat order
		this->tickTargetIndex = 0;
		this->SelectNextCaracter();
		break;
	case CombatPhase::CPHASE_Victory:
		//TODO: handle victory
		break;
	case CombatPhase::CPHASE_GameOver:
		//TODO: handle game over
		break;
	}
}

void CombatEngine::SelectNextCaracter()
{
	this->waitingForCharacter = false;
	for (int i = this->tickTargetIndex; i < this->combatantOrder.Num(); i++)
	{
		UGameCharacter* character = this->combatantOrder[i];

		if (character->HP > 0)
		{
			this->tickTargetIndex = i + 1;
			this->currentTickTarget = character;
			return;
		}
	}

	this->tickTargetIndex = -1;
	this->currentTickTarget = nullptr;
}
